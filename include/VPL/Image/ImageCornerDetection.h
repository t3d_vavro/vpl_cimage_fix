//==============================================================================
/* This file comes from MDSTk software and was modified for
 * 
 * VPL - Voxel Processing Library
 * Changes are Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 * 
 * The original MDSTk legal notice can be found below.
 * 
 * Medical Data Segmentation Toolkit (MDSTk)    
 * Copyright (c) 2003-2007 by PGMed@FIT         
 *
 * Author:  Petr Hyna, xhynap00@stud.fit.vutbr.cz  \n
 * Date:    2007/04/12                          
 * 
 * Description:
 * - Predefined image corner detectors.
 */

#ifndef VPL_ImageCornerDetection_H
#define VPL_ImageCornerDetection_H


//==============================================================================
/*
 * Include all predefined corner detectors.
 */

// General corner detectors
#include "CornerDetection/Harris.h"
#include "CornerDetection/Susan.h"


#endif // VPL_ImageCornerDetection_H

