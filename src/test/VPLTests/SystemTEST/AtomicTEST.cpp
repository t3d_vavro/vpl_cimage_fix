//==============================================================================
/* This file comes from MDSTk software and was modified for
 * 
 * VPL - Voxel Processing Library
 * Changes are Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 * 
 * The original MDSTk legal notice can be found below.
 *
 * Medical Data Segmentation Toolkit (MDSTk) 
 * Copyright (c) 2003-2009 by Michal Spanel  
 *
 * Author:  Michal Spanel, spanel@fit.vutbr.cz  \n
 * Date:    2009/04/20                       
 *
 * Description:
 * - Testing of the vpl::sys::CAtomic class.
 */

#include <VPL/System/Atomic.h>
#include <VPL/System/Sleep.h>
#include <VPL/System/Mutex.h>
#include <VPL/System/ScopedLock.h>
#include <VPL/System/Thread.h>

// STL
#include <iostream>
#include <cstdlib>


//==============================================================================
/*
 * Global constants and variables.
 */

// The number of threads
#define THREADS     5

// Delay after releasing mutex
#define DELAY       50

// An atomic value
vpl::sys::CAtomic   Value;

// Smart pointer to a mutex
vpl::sys::CMutexPtr spMutex;


//==============================================================================
/*!
 * Generates random number
 */
unsigned random(const unsigned uiMax)
{
    return (1 + (unsigned int)((double(rand()) / RAND_MAX) * uiMax));
}


//==============================================================================
/*!
 * Thread routine
 */
VPL_THREAD_ROUTINE(thread)
{
    unsigned id = *(reinterpret_cast<unsigned *>(pThread->getData()));

    VPL_THREAD_MAIN_LOOP
    {
        unsigned val = Value.exchange(id);
        {
            vpl::sys::tScopedLock Guard1(*spMutex);
            std::cout << "Thread: " << id << ",  Old = " << val << std::endl;
        }

        vpl::sys::sleep(random(DELAY));
    }

    vpl::sys::tScopedLock Guard2(*spMutex);
    std::cout << "Thread: " << id << " terminated" << std::endl;

    return 0;
}


//==============================================================================
/*!
 * main
 */
int main(int argc, const char *argv[])
{
    // Threads
    vpl::sys::CThread *pThreads[THREADS];
    int piThreadsId[THREADS];

    // Creation of all ppThreads
    for( int i = 0; i < THREADS; i++ )
    {
//        piThreadsId[i] = i;
        piThreadsId[i] = i * 3;
        pThreads[i] = new vpl::sys::CThread(thread, (void *)&piThreadsId[i], true);
    }

    // Sleep
    vpl::sys::sleep(10000);

    // Destroy all ppThreads
    for( int i = 0; i < THREADS; i++ )
    {
        pThreads[i]->terminate(true, 1000);
        delete pThreads[i];
    }

    return 0;
}

