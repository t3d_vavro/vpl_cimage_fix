//==============================================================================
/* This file comes from MDSTk software and was modified for
 * 
 * VPL - Voxel Processing Library
 * Changes are Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 * 
 * The original MDSTk legal notice can be found below.
 * 
 * Medical Data Segmentation Toolkit (MDSTk) 
 * Copyright (c) 2003-2005 by Michal Spanel  
 *
 * Author:  Michal Spanel, spanel@fit.vutbr.cz  \n
 * Date:    2004/06/01                       
 *
 * Description:
 * - Testing of the vpl::CImage template
 */

#include <VPL/Image/Image.h>
#include <VPL/Image/ImageFunctions.h>

#include "../TestFuncs.h"

// STL
#include <iostream>

//==============================================================================
/*!
 * main
 */
int main(int argc, const char *argv[])
{
    std::cout << "Testing image pixel access overload" << std::endl;
    vpl::img::CDImage im3(1024,1024,8);

    im3.resize(1024, 1024, 8);

    const int count = 100;

    std::cout << "  Basic version" << std::endl;
    vpl::test::tClockCounter start = vpl::test::start();
    for( int c = 0; c < count; ++c )
    {
        const vpl::img::CDImage::tPixel px = vpl::img::CDImage::tPixel(c);
        for( int j = 0; j < im3.getYSize(); ++j )
        {
            for( int i = 0; i < im3.getXSize(); ++i )
            {
                im3(i, j) = px;
            }
        }
    }
    vpl::test::stop(start);
    vpl::test::keypress();

    std::cout << "  Iterator version" << std::endl;
    start = vpl::test::start();
    for( int c = 0; c < count; ++c )
    {
        const vpl::img::CDImage::tPixel px = vpl::img::CDImage::tPixel(c);
        for( vpl::img::CDImage::tIterator it(im3); it; ++it )
        {
            *it = px;
        }
    }
    vpl::test::stop(start);
    vpl::test::keypress();

    std::cout << "  Fill version" << std::endl;
    start = vpl::test::start();
    for( int c = 0; c < count; ++c )
    {
        im3.fill(vpl::img::CDImage::tPixel(c));
    }
    vpl::test::stop(start);
    vpl::test::keypress();

    std::cout << "  Fill entire version" << std::endl;
    start = vpl::test::start();
    for(int c = 0; c < count; ++c )
    {
        im3.fillEntire(vpl::img::CDImage::tPixel(c));
    }
    vpl::test::stop(start);

    return 0;
}
