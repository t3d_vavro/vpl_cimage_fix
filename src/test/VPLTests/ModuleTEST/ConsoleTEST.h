//==============================================================================
/* This file comes from MDSTk software and was modified for
 * 
 * VPL - Voxel Processing Library
 * Changes are Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 * 
 * The original MDSTk legal notice can be found below.
 *  
 * Medical Data Segmentation Toolkit (MDSTk)    \n
 * Copyright (c) 2003-2005 by Michal Spanel     \n
 *
 * Author:  Michal Spanel, spanel@fit.vutbr.cz  \n
 * Date:    2003/11/25                          \n
 *
 * Description:
 * - Testing of the vpl::mod::CConsole class.
 * - Module reads data from input channel a writes them to the output.
 */

#ifndef VPL_CONSOLETEST_H
#define VPL_CONSOLETEST_H

#include <VPL/Base/Setup.h>
#include <VPL/Module/Module.h>


//==============================================================================
/*!
 * Test console application CConsoleTEST.
 */
class CConsoleTEST : public vpl::mod::CModule
{
public:
    //! Smart pointer type
    //! - Declares type tSmartPtr
    VPL_SHAREDPTR(CConsoleTEST);

public:
    //! Default constructor
    CConsoleTEST(const std::string& sDescription);

    //! Virtual destructor
    virtual ~CConsoleTEST();

protected:
    //! Virtual method called on startup
    virtual bool startup();

    //! Virtual method called by the processing thread
    virtual bool main();

    //! Called on console shutdown
    virtual void shutdown();

    //! Called on writing a usage statement
    virtual void writeExtendedUsage(std::ostream& Stream);
};


//==============================================================================
/*!
 * Smart pointer to console application.
 */
typedef CConsoleTEST::tSmartPtr     CConsoleTESTPtr;


#endif // VPL_CONSOLETEST_H

