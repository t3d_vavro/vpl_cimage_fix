//==============================================================================
/* This file is part of
*
* VPL - Voxel Processing Library
* Copyright 2014 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

#include "gtest/gtest.h"
#include <VPL/Module/GlobalSignal.h>


namespace globalSignals
{
//! Methods for register local methods
template <typename T>
T square(T a)
{
	return a * a;
}

template <typename T>
T multiply(T a, T b)
{
	return a * b;
}
template <typename T>
T add(T a, T b, T c)
{
	return a + b + c;
}

//! Test class for register class methods
template <typename T>
class TestClass
{
public:
    //! Default constructor
    TestClass() = default;

	//! Destructor
    virtual ~TestClass() = default;

    T square(T a)
    {
        return a * a;
    }

    T multiply(T a, T b)
    {
        return a * b;
    }

    T add(T a, T b,T c)
    {
        return a + b + c;
    }
};

//! Test fixture
template <typename T>
class GlobalSignalsTest : public ::testing::Test
{
public:
	//! Save type
	using type = T;


	//! Create Signals
    VPL_DECLARE_SIGNAL_1(0, T, T, LocalSigSquare);
    VPL_DECLARE_SIGNAL_2(1, T, T, T, LocalSigMultiply);
	VPL_DECLARE_SIGNAL_3(2, T, T, T,T, LocalSigAdd);

	VPL_DECLARE_SIGNAL_1(3, T, T, ClassSigSquare);
	VPL_DECLARE_SIGNAL_2(4, T, T, T, ClassSigMultiply);
	VPL_DECLARE_SIGNAL_3(5, T, T, T, T, ClassSigAdd);


    void SetUp() override {}
    void TearDown() override {}
};

//! Define types used for typed tests
typedef ::testing::Types<int, unsigned int, double> types;
TYPED_TEST_CASE(GlobalSignalsTest, types);


//! Testing connection to local methods, chekck ids of connection and results of invoking.
TYPED_TEST(GlobalSignalsTest, ConnectLocalMethods)
{
	using type = typename TestFixture::type;

	//! Register a new signal hangler, square<type>()
    vpl::mod::tSignalConnection connectSquare = 
		VPL_SIGNAL(typename TestFixture::LocalSigSquare).connect(square<type>);

	//! Multiple connections
	vpl::mod::tSignalConnection connectMultiply =
		VPL_SIGNAL(typename TestFixture::LocalSigMultiply).connect(multiply<type>);
	vpl::mod::tSignalConnection connectMultiply2 =
		VPL_SIGNAL(typename TestFixture::LocalSigMultiply).connect(multiply<type>);
	vpl::mod::tSignalConnection connectAdd =
		VPL_SIGNAL(typename TestFixture::LocalSigAdd).connect(add<type>);
	vpl::mod::tSignalConnection connectAdd1 =
		VPL_SIGNAL(typename TestFixture::LocalSigAdd).connect(add<type>);
	vpl::mod::tSignalConnection connectAdd2 =
		VPL_SIGNAL(typename TestFixture::LocalSigAdd).connect(add<type>);

	//! Check valid connectionId
	EXPECT_EQ(1, connectSquare.getConnectionId());
	EXPECT_EQ(2, connectMultiply2.getConnectionId());
	EXPECT_EQ(3, connectAdd2.getConnectionId());

	//! Invoke the signal and check result
	EXPECT_EQ(25, VPL_SIGNAL(typename TestFixture::LocalSigSquare).invoke2(5));
	EXPECT_EQ(10, VPL_SIGNAL(typename TestFixture::LocalSigMultiply).invoke2(5,2));
	EXPECT_EQ(111, VPL_SIGNAL(typename TestFixture::LocalSigAdd).invoke2(1,10,100));
}
//! Testing connection to class methods, chekck ids of connection and results of invoking.
TYPED_TEST(GlobalSignalsTest, ConnectClassMethods)
{
	using type = typename TestFixture::type;

    TestClass<type> test;
    

	//! Register a new signal hangler, square<type>()
	vpl::mod::tSignalConnection connectSquare =
		VPL_SIGNAL(typename TestFixture::ClassSigSquare).connect(&test, &TestClass<type>::square);
	//! Multiple connections
	vpl::mod::tSignalConnection connectMultiply =
		VPL_SIGNAL(typename TestFixture::ClassSigMultiply).connect(&test, &TestClass<type>::multiply);
	vpl::mod::tSignalConnection connectMultiply2 =
		VPL_SIGNAL(typename TestFixture::ClassSigMultiply).connect(&test, &TestClass<type>::multiply);
	vpl::mod::tSignalConnection connectAdd =
		VPL_SIGNAL(typename TestFixture::ClassSigAdd).connect(&test, &TestClass<type>::add);
	vpl::mod::tSignalConnection connectAdd1 =
		VPL_SIGNAL(typename TestFixture::ClassSigAdd).connect(&test, &TestClass<type>::add);
	vpl::mod::tSignalConnection connectAdd2 =
		VPL_SIGNAL(typename TestFixture::ClassSigAdd).connect(&test, &TestClass<type>::add);

	//! Check valid connectionId
	EXPECT_EQ(1, connectSquare.getConnectionId());
	EXPECT_EQ(2, connectMultiply2.getConnectionId());
	EXPECT_EQ(3, connectAdd2.getConnectionId());



	//! Invoke the signal and check result
	EXPECT_EQ(25, VPL_SIGNAL(typename TestFixture::ClassSigSquare).invoke2(5));
	EXPECT_EQ(10, VPL_SIGNAL(typename TestFixture::ClassSigMultiply).invoke2(5, 2));
	EXPECT_EQ(111, VPL_SIGNAL(typename TestFixture::ClassSigAdd).invoke2(1, 10, 100));
}

}