//==============================================================================
/* This file is part of
*
* VPL - Voxel Processing Library
* Copyright 2014 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/


#include <gtest/gtest.h>
#include <VPL/Math/Vector.h>
#include <VPL/Test//Compare/compare1D.h>
#include <VPL/Math/Algorithm/FuzzyCMeans.h>
#include "VPL/Test/Accessor/complexString.h"


namespace complex
{
//! Vector of complex numbers.
typedef vpl::math::CVector<vpl::math::CDComplex> tDComplexVector;

//! Test fixture
class ComplexTest : public testing::Test
{
public:
    vpl::math::CDComplex c1;
    vpl::math::CDComplex c2;
    std::stringstream stream;

    vpl::test::Compare1D<vpl::math::CDComplex, vpl::math::CVector<vpl::math::CDComplex>,vpl::test::ComplexString1D> compare;


    void SetUp() override
    {
        c1 = vpl::math::CDComplex(4.0, 3.0);
        c2 = vpl::math::CDComplex(vpl::math::polar(5.0, 0.75));
    }
};

//! Testing initialization of simple complex value.
TEST_F(ComplexTest, Initialize)
{
    stream << c1;
    ASSERT_STREQ("(4,3)", stream.str().c_str());
    stream.str("");

    stream << c2;
    ASSERT_STREQ("(3.65844,3.40819)", stream.str().c_str());
    stream.clear();
}


//! Testing operations on complex numbers.
TEST_F(ComplexTest, Operations)
{
    ASSERT_NEAR(5, vpl::math::getAbs(c1), 3);
    ASSERT_NEAR(25, vpl::math::getNorm(c1), 3);
    ASSERT_NEAR(0.643501, vpl::math::getArg(c1), 3);

    stream << c1.getConj();
    ASSERT_STREQ("(4,-3)", stream.str().c_str());
    stream.str("");

    stream << 4.4 + c1 * 1.8;
    ASSERT_STREQ("(11.6,5.4)", stream.str().c_str());
    stream.str("");
    stream << c1 + c2;
    ASSERT_STREQ("(7.65844,6.40819)", stream.str().c_str());
    stream.str("");
}

//! Testing operations which used vetors.
TEST_F(ComplexTest, Vector)
{
    tDComplexVector v1(4);
    v1.fill(vpl::math::CDComplex(1, 0.5));
    compare.valuesString("(1,0.5)", v1,v1.size());

    tDComplexVector v2(4);
    for (vpl::tSize j = 0; j < v2.getSize(); j++)
    {
        v2(j) = j;
    }
    std::string req[4] = {"(0,0)","(1,0)" ,"(2,0)" ,"(3,0)" };
    compare.valuesString(req, v2, v2.size());

    v1 += v2;
    std::string req2[4] = { "(1,0.5)","(2,0.5)" ,"(3,0.5)" ,"(4,0.5)" };
    compare.valuesString(req2, v1, v1.size());

    tDComplexVector::tSmartPtr spV3(new tDComplexVector(8));
    spV3->fill(vpl::math::CDComplex(0, 0));
    compare.valuesString("(0,0)", *spV3, spV3->size());
    spV3->fill(vpl::math::CDComplex(1, 3));
    compare.valuesString("(1,3)", *spV3, spV3->size());

    stream << vpl::math::getSum<vpl::math::CDComplex>(*spV3);
    ASSERT_STREQ("(8,24)", stream.str().c_str());
    stream.str("");
    stream << vpl::math::getMean<vpl::math::CDComplex>(*spV3);
    ASSERT_STREQ("(1,3)", stream.str().c_str());
    stream.str("");
    v1.subSample(*spV3, 2);
    compare.valuesString("(1,3)", v1, v1.size());

    compare.valuesString(req, v2, v2.size());

    spV3->concat(v1, v2);
    std::string req3[8] = { "(1,3)", "(1,3)", "(1,3)", "(1,3)", "(0,0)", "(1,0)", "(2,0)", "(3,0)" };
    compare.valuesString(req3, *spV3, spV3->size());
}
}
