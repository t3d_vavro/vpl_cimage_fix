//==============================================================================
/* This file is part of
*
* VPL - Voxel Processing Library
* Copyright 2014 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

#include "gtest/gtest.h"
#include <VPL/Math/Vector.h>
#include <VPL/Math/Quaternion.h>
#include <VPL/Math/MatrixFunctions.h>
#include <VPL/Test/Compare/compare2D.h>
#include <VPL/Test/Compare/compare1D.h>
#include <VPL/Test/printDefinition.h>
#include "VPL/Test/Accessor/AccessorAtAbs.h"

namespace matrix
{
// Define Test fixture as typed test. It will be runned for int, float, double ...)
template <class T>
class MatrixTest : public testing::Test
{
public:
    // Save type for testing
    using type = T;

    vpl::math::CMatrix<type> m1;
    vpl::math::CMatrix<type> m2;

    //! For testing value by value, is created comparator, where: 
    //! 1 template argument type of simple value (tDensityPixel, float ...)
    //! 2 template argument is type of object which will be tested.
    //! 3 template arguments which is defauilt AccessorAt. It is class which define how is one value from object getted.
    vpl::test::Compare2D<type, vpl::math::CMatrix<type>> compare2D;
    vpl::test::Compare1D<type, vpl::math::CVector<type>> compare1D;


    void SetUp() override
    {
        m1 = vpl::math::CMatrix<type>(4, 4);
        m2 = vpl::math::CMatrix<type>(4, 4);

        compare1D.setErrorMessage("Matrix is different at index");
        compare1D.setArrayOrder(vpl::test::ArrayOrder::COLUMN_MAJOR);
        compare2D.setErrorMessage("Matrix is different at index");
        compare2D.setArrayOrder(vpl::test::ArrayOrder::COLUMN_MAJOR);

    }

	//! Initialize matrix 0 - 15
    void initializeMatrix0to15(vpl::math::CMatrix<type>& m)
    {
        for (vpl::tSize i = 0; i < m.getNumOfRows(); i++)
        {
            for (vpl::tSize j = 0; j < m.getNumOfCols(); j++)
            {
                m(i, j) = i * m.getNumOfRows() + j;
            }
        }
    }
    void initializeMatrixType1(vpl::math::CMatrix<type>& m)
    {
        m(0, 0) = 2; m(0, 1) = 1; m(0, 2) = 0; m(0, 3) = 0; m(0, 4) = 0;
        m(1, 0) = 1; m(1, 1) = 2; m(1, 2) = 1; m(1, 3) = 0; m(1, 4) = 0;
        m(2, 0) = 0; m(2, 1) = 1; m(2, 2) = 2; m(2, 3) = 1; m(2, 4) = 0;
        m(3, 0) = 0; m(3, 1) = 0; m(3, 2) = 1; m(3, 3) = 2; m(3, 4) = 1;
        m(4, 0) = 0; m(4, 1) = 0; m(4, 2) = 0; m(4, 3) = 1; m(4, 4) = 2;
    }
};

typedef testing::Types< double, float> Implementations;
TYPED_TEST_CASE(MatrixTest, Implementations);


//! Test for initialize by unit matrix
TYPED_TEST(MatrixTest, Initialize)
{
    using type = typename TestFixture::type;

    type test[] = { 1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1 };
    TestFixture::m1.unit();
    TestFixture::compare2D.values(test, TestFixture::m1, 4, 4);
}

//! Testing set of matrix.
TYPED_TEST(MatrixTest, SetMatrix)
{
	using type = typename TestFixture::type;


    TestFixture::compare2D.setArrayOrder(vpl::test::ArrayOrder::COLUMN_MAJOR);
    TestFixture::initializeMatrix0to15(TestFixture::m1);
    type test[] = { 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15 };
    TestFixture::compare2D.values(test, TestFixture::m1, 4, 4);

    vpl::math::CMatrix<type> m5(5, 5), m55(5, 5);
    TestFixture::initializeMatrixType1(m5);

    type test2[] = { 2,1,0,0,0, 1,2,1,0,0, 0,1,2,1,0, 0,0,1,2,1, 0,0,0,1,2 };
    TestFixture::compare2D.values(test2, m5, 5, 5);

}

//! Test for deteriminant
TYPED_TEST(MatrixTest, GetDeterminant)
{
	using type = typename TestFixture::type;

    vpl::math::CMatrix<type> m5(5, 5), m55(5, 5);
    TestFixture::initializeMatrixType1(m5);

    type dDet = vpl::math::getDeterminant<type>(m5);
    ASSERT_FLOAT_EQ(6, dDet) << "Determinant is different.";
}

//! Testing Matrix operation eig
TYPED_TEST(MatrixTest, OperationEig)
{
    TestFixture::compare2D.enableAssert(false);

	using type = typename TestFixture::type;



    vpl::math::CMatrix<type> m5(5, 5), m55(5, 5);
    TestFixture::initializeMatrixType1(m5);

    vpl::math::CVector<type> v(5);
    vpl::math::eig(m5, v);

	vpl::test::Compare2D<type, vpl::math::CMatrix<type>,vpl::test::AccessorAtAbs2D> compare2DAbs;

    type test[] = { 0.288675, 0.500000, 0.577350, 0.500000, 0.288675,
        0.500000, 0.500000, 0.000000, 0.500000, 0.500000,
        0.577350, 0.000000, 0.577350, 0.000000, 0.577350,
        0.500000, 0.500000, 0.000000, 0.500000, 0.500000,
        0.288675, 0.500000, 0.577350, 0.500000, 0.288675 };
    compare2DAbs.enablePrintData(true);
    compare2DAbs.values(test, m5, 5, 5);

    type test2[] = { 0.267949, 1.000000, 2.000000, 3.000000, 3.732051 };
    TestFixture::compare1D.values(test2, v, 5);


}

//! Testing iteration by iterator
TYPED_TEST(MatrixTest, Iterator)
{
	using type = typename TestFixture::type;

    TestFixture::initializeMatrix0to15(TestFixture::m1);
    int requiredValue = 0;
    for (typename vpl::math::CMatrix<type>::tIterator It(TestFixture::m1); It; ++It)
    {
        EXPECT_FLOAT_EQ(requiredValue++, *It);
    }
}

//! Testing several operations like multipy by scalar ...
TYPED_TEST(MatrixTest, Operations)
{
	using type = typename TestFixture::type;

    TestFixture::m1.unit();
    TestFixture::initializeMatrix0to15(TestFixture::m2);
    TestFixture::m1 += TestFixture::m2;
    type test[] = 
    { 1,1,2,3, 4,6,6,7, 8,9,11,11, 12,13,14,16 };

    TestFixture::compare2D.values(test, TestFixture::m1, 4, 4);

    TestFixture::m2 *= vpl::CScalar<type>(2);
    type test2[] = 
    { 0,2,4,6, 8,10,12,14, 16,18,20,22, 24,26,28,30 };
    TestFixture::compare2D.values(test2, TestFixture::m2, 4, 4);


    vpl::math::CMatrix<type> m3;
    m3.asEigen() = TestFixture::m1.asEigen().block(1, 1, 2, 3);
    m3.resize(4, 4);
    m3.mult(TestFixture::m1, TestFixture::m2);
    type test3[] = 
    { 112,126,140,154, 312,358,404,450, 512,590,668,746, 712,822,932,1042 };

    TestFixture::compare2D.values(test3, m3, 4, 4);

    for (vpl::tSize i = 0; i < TestFixture::m1.getNumOfRows(); i++)
    {
        for (vpl::tSize j = 0; j < TestFixture::m1.getNumOfCols(); j++)
        {
            TestFixture::m1(i, j) += i * TestFixture::m1.getNumOfCols() + j;
        }
    }

    type test4[] = { 1,2,4,6, 8,11,12,14, 16,18,21,22, 24,26,28,31 };
    TestFixture::compare2D.values(test4, TestFixture::m1, 4, 4);

    TestFixture::m2.transpose(TestFixture::m1);

    type test5[] = { 1,8,16,24, 2,11,18,26, 4,12,21,28, 6,14,22,31 };
    TestFixture::compare2D.values(test5, TestFixture::m2, 4, 4);


    type dDet = vpl::math::getDeterminant<type>(TestFixture::m2);
    ASSERT_FLOAT_EQ(-259, dDet) << "Determinant dont match.";

    TestFixture::m1 = TestFixture::m2;
    vpl::math::inverse(TestFixture::m1);

    type test6[] = { 0.135135, -0.463320, -0.061776, 0.339768,
                    -0.486486, 0.667954, -0.177606, -0.023166,
                    -0.108108, -0.200772, 0.706564, -0.386100,
                    0.270270, -0.069498, -0.409266, 0.250965 };
    TestFixture::compare2D.values(test6, TestFixture::m1, 4, 4);

    type test7[] = { 1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1 };

    m3.mult(TestFixture::m1, TestFixture::m2);
    TestFixture::compare2D.values(test7, m3, 4, 4);


    vpl::math::CMatrix<type> m4;

    m4.copy(TestFixture::m1, 1, 1, 2, 2);
    type test8[] = {
         0.667954, -0.177606,
         -0.200772, 0.706564, };

    TestFixture::compare2D.values(test8, m4, 2, 2);

    for (int i = 0; i < 4; i++)
    {
        test8[i] = test8[i] * 10;
    }
    m4 *= vpl::CScalar<int>(10);

    TestFixture::compare2D.values(test8, m4, 2, 2);


}

//! Testing method block
TYPED_TEST(MatrixTest, Block)
{
	using type = typename TestFixture::type;

    TestFixture::compare2D.setArrayOrder(vpl::test::ArrayOrder::COLUMN_MAJOR);
    TestFixture::initializeMatrix0to15(TestFixture::m1);
    vpl::math::CMatrix<type> m3;
    m3.asEigen() = TestFixture::m1.asEigen().block(1, 1, 2, 3);

    type test[] = { 5,6,7,9,10,11 };

    TestFixture::compare2D.values(test, m3, m3.getNumOfRows(), m3.getNumOfCols());
}
}
