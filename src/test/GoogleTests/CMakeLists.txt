#################################################################################
# This file is part of
#
# VPL - Voxel Processing Library
# Copyright 2014 3Dim Laboratory s.r.o.
# All rights reserved.
#
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
################################################################################
MACRO( VPL_TEST_INCLUDE_DIR _DIR )
    FILE( GLOB_RECURSE VPL_HEADERS ${_DIR}/*.h ${_DIR}/*.hxx )
ENDMACRO( VPL_TEST_INCLUDE_DIR )
  
  
if(GTEST_FOUND OR AddGTest_FOUND)
enable_testing()

# VPL libraries
set( VPL_LIBS ${VPL_IMAGEIO_LIB} ${VPL_IMAGE_LIB} ${VPL_IMAGEIO_LIB} ${VPL_MODULE_LIB} ${VPL_SYSTEM_LIB} ${VPL_MATH_LIB} ${VPL_BASE_LIB} )
# VPL GTest helper Library
set( VPL_GTEST_LIB ${VPL_LIBRARY_PREFIX}Test${VPL_LIBRARY_SUFFIX} )
set( VPL_LIBS ${VPL_LIBS} ${VPL_GTEST_LIB} )
  
# TinyXML
if( VPL_XML_ENABLED AND TINYXML_FOUND )
    INCLUDE_DIRECTORIES(${TINYXML_INCLUDE_DIR})
    LINK_DIRECTORIES(${TINYXML_LIBRARIES_DIR})
endif( VPL_XML_ENABLED AND TINYXML_FOUND)
  
INSTALL(DIRECTORY ${CMAKE_CURRENT_LIST_DIR}/data/ DESTINATION "test/gtest/data")
INSTALL(FILES ${CMAKE_CURRENT_LIST_DIR}/run.bat DESTINATION "test/gtest")


ADD_SUBDIRECTORY( Math )
ADD_SUBDIRECTORY( Image )
ADD_SUBDIRECTORY( Base )
ADD_SUBDIRECTORY( Module )
ADD_SUBDIRECTORY( System )
endif()


  
