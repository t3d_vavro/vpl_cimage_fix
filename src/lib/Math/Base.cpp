//==============================================================================
/* This file comes from MDSTk software and was modified for
 * 
 * VPL - Voxel Processing Library
 * Changes are Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 * 
 * The original MDSTk legal notice can be found below.
 *  
 * Medical Data Segmentation Toolkit (MDSTk) 
 * Copyright (c) 2003-2006 by Michal Spanel  
 *
 * Author:  Michal Spanel, spanel@fit.vutbr.cz  \n
 * Date:    2003/10/16                       
 * 
 * Description:
 * - Definition of various constants and macros.
 */

#include <VPL/Math/Base.h>


namespace vpl
{
namespace math
{

//=============================================================================
/*
 * Various definitions.
 */

//! Dummy value.
const int DUMMY_CONSTANT    = 0;

//! Dummy function.
int dummyFunc()
{
    return 0;
}


} // namespace math
} // namespace vpl

