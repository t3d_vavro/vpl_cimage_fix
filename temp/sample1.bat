@echo off 
rem Please, modify and run the 'wsetenv.bat' batch file first in order to
rem setup path to built binaries, etc.

echo Image FFT...
mdsLoadDicom <../data/dicom/80.dcm |mdsSliceFFT -result abs -shift -logspectrum |mdsSliceRange -auto |mdsSliceView

