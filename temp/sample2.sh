#!/bin/sh
# Please, modify and run either the 'lsetenv.sh' or the 'msetenv.sh' shell
# script first in order to setup path to built binaries, etc.

echo "Image segmentation using the FCM clustering technique..."
mdsLoadDicom <../data/dicom/80.dcm |mdsSliceSegFCM -clusters 3 |mdsSliceView -coloring segmented
